package com.postnord.ut;

import com.postnord.kafkaManager.CreditCard;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.Set;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CreditCardTest {

    CreditCard credit;
    Validator validator;


    @BeforeEach
    public void init() {
        credit = new CreditCard("Dev Dutha", "1111-2222-3333-4444", 5, 2023, 123, 50000F);
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @AfterEach
    public void tearDown() {
        credit = null;
    }

    @Test
    @Order(1)
    public void whenValidCardNumber_thenNoExceptionThrown() {
        assertDoesNotThrow(() -> {
            credit.setCardNumber("2222-3333-4444-5555");
        });
    }

    @Test
    @Order(2)
    public void whenInvalidCardNumber_thenViolation() {
        credit.setCardNumber("2222-3333-4444-55555");
        Set<ConstraintViolation<CreditCard>> violations
                = validator.validate(credit);
        assertEquals(violations.size(), 1);
    }

    @Test
    @Order(4)
    public void whenSecurityCodeIsExceeded_thenViolation() {
        credit.setSecurityCode(1001);
        Set<ConstraintViolation<CreditCard>> violations
                = validator.validate(credit);
        assertEquals(violations.size(), 1);
    }

}
