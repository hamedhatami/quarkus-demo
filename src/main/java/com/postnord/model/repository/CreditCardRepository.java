package com.postnord.model.repository;


import com.postnord.kafkaManager.CreditCard;
import com.postnord.model.entity.CreditCardEntity;

import java.util.List;

import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CreditCardRepository {

    @WithTransaction
    public Uni<Void> addCreditCard(final CreditCard creditCardDto) {

        var creditCardEntity = new CreditCardEntity();
        creditCardEntity.availableCredit = creditCardDto.getAvailableCredit();
        creditCardEntity.cardNumber = creditCardDto.getCardNumber();
        creditCardEntity.ownerName = creditCardDto.getOwnerName();
        creditCardEntity.expirationMonth = creditCardDto.getExpirationMonth();
        creditCardEntity.expirationYear = creditCardDto.getExpirationYear();
        creditCardEntity.securityCode = creditCardDto.getSecurityCode();

        return creditCardEntity
                .persistAndFlush()
                .replaceWith(Uni
                        .createFrom()
                        .voidItem())
                .onFailure()
                .transform(t -> new IllegalStateException("Error"));


    }


    @WithTransaction
    public Uni<List<CreditCardEntity>> creditCardList() {
        return CreditCardEntity.listAll();
    }

}
