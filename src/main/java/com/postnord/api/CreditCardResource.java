package com.postnord.api;


import com.postnord.kafkaManager.CreditCard;
import com.postnord.kafkaManager.EventProducer;
import com.postnord.model.repository.CreditCardRepository;

import java.util.logging.Logger;

import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


@Path("/credit")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CreditCardResource {

    private static final Logger LOG = Logger.getLogger(CreditCardResource.class.getName());

    @Inject
    EventProducer eventProducer;
    @Inject
    CreditCardRepository creditCardRepository;

    @POST
    public Uni<Response> createCredit(@Valid final CreditCard creditCardDto) {
        return eventProducer.produceEvent(creditCardDto);
    }

    @GET
    public Uni<Response> CreditList() {

        LOG.info("hello");

        return creditCardRepository
                .creditCardList()
                .onItem()
                .transform(item -> Response
                        .ok(item)
                        .build());
    }
}
