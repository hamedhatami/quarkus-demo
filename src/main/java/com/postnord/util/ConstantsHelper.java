package com.postnord.util;

import java.nio.charset.StandardCharsets;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;

public final class ConstantsHelper {

    private ConstantsHelper() {
    }

    public static final Jsonb JSON_B = JsonbBuilder.create(new JsonbConfig()
            .withNullValues(true)
            .withEncoding(StandardCharsets.UTF_8.name()));
}
