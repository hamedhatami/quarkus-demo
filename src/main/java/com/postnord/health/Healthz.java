package com.postnord.health;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;

import jakarta.enterprise.context.ApplicationScoped;


@Liveness
@Readiness
@ApplicationScoped
public class Healthz implements HealthCheck {

    @Override
    public HealthCheckResponse call() {

        return HealthCheckResponse
                .named("quarkus-demo")
                .up()
                .build();
    }
}