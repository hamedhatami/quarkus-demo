package com.postnord.kafkaManager;

import com.postnord.kafkaManager.interceptor.RunInSafeContext;
import com.postnord.model.repository.CreditCardRepository;

import org.eclipse.microprofile.reactive.messaging.Acknowledgment;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class EventConsumer {
    @Inject
    CreditCardRepository creditCardRepository;

    @Incoming("event-in")
    @Acknowledgment(Acknowledgment.Strategy.POST_PROCESSING)
    @RunInSafeContext
    public Uni<Void> consumeEvent(final CreditCard creditCardDto) {
        Uni<Void> result = creditCardRepository.addCreditCard(creditCardDto);
        return result;
    }
}
