package com.postnord.kafkaManager.handler;

import com.postnord.kafkaManager.CreditCard;

import org.eclipse.microprofile.reactive.messaging.Acknowledgment;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;

import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.kafka.api.IncomingKafkaRecordMetadata;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DeadLetterTopicReader {
    @SuppressWarnings("unchecked")
    @Incoming("dead-letter-topic-event-out")
    @Acknowledgment(Acknowledgment.Strategy.POST_PROCESSING)
    public Uni<Void> dead(Message<CreditCard> rejected) {
        IncomingKafkaRecordMetadata<String, String> metadata = rejected.getMetadata(IncomingKafkaRecordMetadata.class)
                .orElseThrow(() -> new IllegalArgumentException("Expected a message coming from Kafka"));
        String reason = new String(metadata.getHeaders().lastHeader("dead-letter-reason").value());
        System.out.println(String.format("The message '%s' has been rejected and sent to the DLT. The reason is: '%s'.", rejected.getPayload(), reason));

        return Uni.createFrom().voidItem();
    }
}
