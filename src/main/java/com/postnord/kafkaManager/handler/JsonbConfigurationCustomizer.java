package com.postnord.kafkaManager.handler;

import java.nio.charset.StandardCharsets;

import io.quarkus.jsonb.JsonbConfigCustomizer;
import jakarta.inject.Singleton;
import jakarta.json.bind.JsonbConfig;


@Singleton
public class JsonbConfigurationCustomizer implements JsonbConfigCustomizer {

    @Override
    public void customize(JsonbConfig config) {
        config
                .withNullValues(true)
                .withEncoding(StandardCharsets.UTF_8.name());
    }
}
